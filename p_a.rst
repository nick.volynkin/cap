Распределенность и доступность
==============================

..  include:: availability.rst

..  include:: non_consistency.rst

..  include:: partitioning.rst
