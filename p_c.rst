Распределенность и консистентность
==================================

..  include:: non_availability.rst

..  include:: consistency.rst

..  include:: partitioning.rst
