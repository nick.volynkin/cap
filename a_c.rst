Доступность и консистентность
=============================


..  include:: availability.rst

..  include:: consistency.rst

..  include:: non_partitioning.rst
